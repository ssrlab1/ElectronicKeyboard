<?php
	class ElectronicKeyboard {
		private static $localizationArr = array();
		private static $localizationErr = array();
		const BR = "<br>\n";
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Electronic Keyboard';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/ElectronicKeyboard/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		static public function getCharacterClasses() {
			$characterClasses = array();
			$filePath = dirname(__FILE__) . '/base/CharacterClasses.txt';
			$handle = fopen($filePath, 'r') OR die('fail open CharacterClasses.txt');
			if($handle) {
				while(($buffer = fgets($handle, 4096)) !== false) {
					if(substr($buffer, 0, 1) != '#' && substr($buffer, 0, 2) != '//') {
						$lineArr = preg_split("/\t/", $buffer);
						if(isset($lineArr[0]) && isset($lineArr[1]) && isset($lineArr[2])  && isset($lineArr[3]) && isset($lineArr[4])) {
							$characterClasses[] = array('first' => $lineArr[0], 'last' => $lineArr[1], 'en' => $lineArr[2], 'be' => $lineArr[3], 'ru' => $lineArr[4]);
						}
					}
				}
			}
			fclose($handle);
			return $characterClasses;
		}
	}
?>