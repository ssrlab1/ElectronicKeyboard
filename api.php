<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$msg = '';
	include_once 'ElectronicKeyboard.php';
	$action = isset($_GET['action']) ? $_GET['action'] : '';
	$number = isset($_GET['number']) ? $_GET['number'] : 0;
	$localization = isset($_GET['localization']) ? $_GET['localization'] : 'en';
	ElectronicKeyboard::loadLocalization($localization);
	
	if($action == 'loadList') {
		$characterClasses = ElectronicKeyboard::getCharacterClasses();
		foreach($characterClasses as $class) {
			$result['list'][] = $class[$localization];
		}
		$msg = json_encode($result);
	}
	elseif($action == 'loadClass') {
		$characterClasses = ElectronicKeyboard::getCharacterClasses();
		$result['title'] = $characterClasses[$number][$localization];
		$result['first'] = $characterClasses[$number]['first'];
		$result['last'] = $characterClasses[$number]['last'];
		$msg = json_encode($result);
	}
	echo $msg;
?>