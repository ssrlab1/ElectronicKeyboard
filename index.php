<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'ElectronicKeyboard.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = ElectronicKeyboard::loadLanguages();
	ElectronicKeyboard::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo ElectronicKeyboard::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', ElectronicKeyboard::showMessage('default input'))); ?>";
			$(document).ready(function () {
				$.ajax({
					type: 'GET',
					url: 'https://corpus.by/ElectronicKeyboard/api.php',
					data: {
						'action': 'loadList',
						'localization': '<?php echo $lang; ?>'
					},
					success: function(msg){
						var result = jQuery.parseJSON(msg);
						var output = '';
						// output += '<input type="checkbox" name="characterClasses"> ' + result.list[key] + '</input>&nbsp;&nbsp;&nbsp;';
						output += '<select name="selector" id="selectorId" class="selector-primary">';					
						for (var key in result.list) {
							if (typeof result.list[key] !== 'function') {
								if(key == 5) {
									output += '<option value="option'+key+'" selected>'+result.list[key]+'</option>';
								}
								else {
									output += '<option value="option'+key+'">'+result.list[key]+'</option>';
								}
							}
						}
						output += '</select>';
						$('#characterClassesId').html(output);
						loadClass(5);
					},
					error: function(){
						$('#characterClassesId').html('ERROR');
					}
				});
				
				$(document).on('change', '#selectorId', function(){
					var number = $(this).val().substring(6);
					loadClass(number);
				});
				
				$(document).on("click", "#copyButton", function() {
					copyToClipboard(document.getElementById("inputTextId"));
				});
				
				function loadClass(number) {
					$.ajax({
						type: 'GET',
						url: 'https://corpus.by/ElectronicKeyboard/api.php',
						data: {
							'action': 'loadClass',
							'number': number,
							'localization': '<?php echo $lang; ?>'
						},
						success: function(msg){
							var result = jQuery.parseJSON(msg);
							var output = '';
							//output = '<h3>' + result.title + '</h3>';
							for(var dec = parseInt(result.first); dec <= parseInt(result.last); dec++) {
								var hex = dec.toString(16);
								hex = hex.padStart(4, "0");
								output += '<input class="symbol-button" value="&#' + dec + '" type="button" onclick="InsertCodeInTextArea(\'inputTextId\', \'\\u' + hex + '\');">';
							}
							$('#charactersId').html(output);
						},
						error: function(){
							$('#charactersId').html('ERROR');
						}
					});
				}
				
				function copyToClipboard(elem) {
					// create hidden text element, if it doesn't already exist
					var targetId = "_hiddenCopyText_";
					var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
					var origSelectionStart, origSelectionEnd;
					if (isInput) {
						// can just use the original source element for the selection and copy
						target = elem;
						origSelectionStart = elem.selectionStart;
						origSelectionEnd = elem.selectionEnd;
					} else {
						// must use a temporary form element for the selection and copy
						target = document.getElementById(targetId);
						if (!target) {
							var target = document.createElement("textarea");
							target.style.position = "absolute";
							target.style.left = "-9999px";
							target.style.top = "0";
							target.id = targetId;
							document.body.appendChild(target);
						}
						target.textContent = elem.textContent;
					}
					// select the content
					var currentFocus = document.activeElement;
					target.focus();
					target.setSelectionRange(0, target.value.length);
					
					// copy the selection
					var succeed;
					try {
						  succeed = document.execCommand("copy");
					} catch(e) {
						succeed = false;
					}
					// restore original focus
					if (currentFocus && typeof currentFocus.focus === "function") {
						currentFocus.focus();
					}
					
					if (isInput) {
						// restore prior selection
						elem.setSelectionRange(origSelectionStart, origSelectionEnd);
					} else {
						// clear temporary content
						target.textContent = "";
					}
					return succeed;
				}
			});
			
			function InsertCodeInTextArea(textAreaId, textValue) {
				//Get textArea HTML control 
				var txtArea = document.getElementById(textAreaId);
				//IE
				if (document.selection) {
					txtArea.focus();
					var sel = document.selection.createRange();
					sel.text = textValue;
					return;
				}
				//Firefox, chrome, mozilla
				else if (txtArea.selectionStart || txtArea.selectionStart == '0') {
					var startPos = txtArea.selectionStart;
					var endPos = txtArea.selectionEnd;
					var scrollTop = txtArea.scrollTop;
					txtArea.value = txtArea.value.substring(0, startPos) + textValue + txtArea.value.substring(endPos, txtArea.value.length);
					txtArea.focus();
					txtArea.selectionStart = startPos + textValue.length;
					txtArea.selectionEnd = startPos + textValue.length;
				}
				else {
					txtArea.value += textArea.value;
					txtArea.focus();
				}
			}
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/ElectronicKeyboard/?lang=<?php echo $lang; ?>"><?php echo ElectronicKeyboard::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo ElectronicKeyboard::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo ElectronicKeyboard::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = ElectronicKeyboard::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . ElectronicKeyboard::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo ElectronicKeyboard::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo ElectronicKeyboard::showMessage('clear'); ?></button>
									<button type="button" class="btn btn-default btn-xs" id="copyButton"><?php echo ElectronicKeyboard::showMessage('copy'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo ElectronicKeyboard::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", ElectronicKeyboard::showMessage('default input')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div id="characterClassesId"></div>
				</div>
				<div class="col-md-12">
					<div id="charactersId"></div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo ElectronicKeyboard::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/ElectronicKeyboard" target="_blank"><?php echo ElectronicKeyboard::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/ElectronicKeyboard/-/issues/new" target="_blank"><?php echo ElectronicKeyboard::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo ElectronicKeyboard::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo ElectronicKeyboard::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo ElectronicKeyboard::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php ElectronicKeyboard::sendErrorList($lang); ?>