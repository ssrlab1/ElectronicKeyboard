title
Электронная клавіятура

help
https://ssrlab.by/by/8198

be
Беларуская

ru
Русский

en
English

input
Калі ласка, увядзіце тэкст

refresh
Абнавіць

clear
Ачысціць

copy
Капіраваць

default input
[bʲiblʲijaˈtɛka]

service code
Зыходны код сэрвіса можна спампаваць па

reference
спасылцы

suggestions
Прапанаваць паляпшэнне працы сэрвіса

contact e-mail
Мы будзем рады атрымаць зваротную сувязь ад Вас на e-mail

other prototypes
Нашы іншыя прататыпы:

laboratory
Лабараторыя распазнавання і сінтэзу маўлення, АІПІ НАН Беларусі
