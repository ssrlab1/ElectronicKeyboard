title
Электронная клавиатура

help
https://ssrlab.by/ru/8198

be
Беларуская

ru
Русский

en
English

input
Пожалуйста, введите текст

refresh
Обновить

clear
Очистить

copy
Копировать

default input
[bʲiblʲijaˈtɛka]

service code
Исходный код сервиса можно скачать по

reference
ссылке

suggestions
Предложить улучшение работы сервиса

contact e-mail
Мы будем рады получить обратную связь от Вас на e-mail

other prototypes
Другие наши прототипы:

laboratory
Лаборатория распознавания и синтеза речи, ОИПИ НАН Беларуси
